# Relay

A PSR-7 middleware dispatcher.

This package is installable and PSR-4 autoloadable via Composer as `relay/relay`.

Alternatively, download a release or clone this repository, then map the `Relay\` namespace to the package `src/` directory.

This package requires PHP 5.5 or later; it has been tested on PHP 5.6, PHP 7, and HHVM. You should use the latest available version of PHP as a matter of principle.

To run the tests, issue `composer install` to install the test dependencies, then issue `phpunit`.

Please see <http://relayphp.github.io> for documentation.
